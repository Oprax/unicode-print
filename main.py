import flask as fl


app = fl.Flask(__name__)


@app.route("/")
def index():
    ctx = {"ex_url": fl.url_for("u", codepoints="1f383/1F480/1f47B", sep="|-|")}
    return fl.render_template("index.html", **ctx)


@app.route("/u/<path:codepoints>")
def u(codepoints):
    codepoints = [chr(int(c, base=16)) for c in codepoints.split("/")]
    sep = fl.request.args.get("sep", "")
    resp = fl.Response(sep.join(codepoints))
    resp.headers["Content-Type"] = "text/plain; charset=utf-8"
    resp.headers["Access-Control-Allow-Origin"] = "*"
    return resp
